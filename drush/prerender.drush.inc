<?php
use Drupal\Core\Queue\QueueWorkerInterface;
use Drupal\Core\Queue\RequeueException;
use Drupal\Core\Queue\SuspendQueueException;

/**
 * Drush commands.
 * This should later be ported to the drupal console,
 * but the console still has some problems.
 */
function prerender_drush_command() {
  $items['prerender-crawl'] = array(
    'description' => dt('Crawl the site directly without using cron.'),
    'drush dependencies' => array(),
    'arguments' => array(
      'path' => dt('The path to start with.'),
    ),
    'options' => array(
      'max-seconds' => dt('Maximum amount of seconds to run this command.')
    ),
    'required-arguments' => 1,
    'aliases' => array('pr-cr'),
  );
  return $items;
}

function drush_prerender_prerender_crawl($seed) {
  $container = \Drupal::getContainer();
  // Queue manager.
  $queue_manager = $container->get('plugin.manager.queue_worker');
  /** @var \Drupal\prerender\PrerenderServiceInterface $service */
  $service = $container->get('prerender.prerender');

  // get the path validator.
  /** @var \Drupal\Core\Path\PathValidatorInterface $path_validator */
  $path_validator = $container->get('path.validator');

  // Be sure to set the crawking enabled.
  $service->setCrawlingEnabled(TRUE);

  if ($url = $path_validator->getUrlIfValid($seed)) {
    $max_seconds = (drush_get_option('max-seconds') > 0) ?  drush_get_option('max-seconds') : 0;
    $end = time() + $max_seconds;
    drush_log(dt('Starting crawling with seed: @seed and max-seconds: @maxtime', [
      '@seed' => '/' . $url->getInternalPath(),
      '@maxtime' => $max_seconds,
    ]), 'ok');

  // Try to get the item with the seed.
    $prerendered_data = $service->getData('/' . $url->getInternalPath());
    $service->queueData($prerendered_data, TRUE);

    /** @var \Drupal\Core\Queue\QueueFactory $queue_factory */
    $queue_factory = $container->get('queue');
    // Make sure every queue exists. There is no harm in trying to recreate
    // an existing queue.
    $queue_name = 'prerender_cron_prerenderer';
    $queue_factory->get($queue_name)->createQueue();

    $queue = $queue_factory->get($queue_name);
    /** @var QueueWorkerInterface $queue_worker */
    $queue_worker = $queue_manager->createInstance($queue_name);
    while (($max_seconds === 0 || time() < $end) && ($item = $queue->claimItem())) {
      try {
        drush_log(dt('Prerendering item with id @id', ['@id' => $item->data->id]), 'ok');
        $queue_worker->processItem($item->data);
        $queue->deleteItem($item);
      }
      catch (RequeueException $e) {
        // The worker requested the task be immediately requeued.
        $queue->releaseItem($item);
      }
      catch (SuspendQueueException $e) {
        // If the worker indicates there is a problem with the whole queue,
        // release the item and skip to the next queue.
        $queue->releaseItem($item);

        watchdog_exception('cron', $e);
      }
      catch (\Exception $e) {
        // In case of any other kind of exception, log it and leave the item
        // in the queue to be processed again later.
        watchdog_exception('prerender_drush_crawl', $e);
      }
    }
  }
  else {
    drush_log(dt('Seed @seed is not a valid path.', array('@seed' => $seed)), 'error');
  }
}