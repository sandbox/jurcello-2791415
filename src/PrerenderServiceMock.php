<?php

namespace Drupal\prerender;

use Drupal\Core\Cache\Cache;
use Drupal\prerender\Entity\PrerenderedDataInterface;
use Drupal\node\Entity\Node;


/**
 * Class PrerenderService.
 *
 * @package Drupal\prerender
 */
class PrerenderServiceMock extends PrerenderServiceBase implements PrerenderServiceInterface {

  public function getPrerenderedHtml($path) {
    // TODO: Implement getPrerenderedHtml() method.
  }

  public function invalidatePath($path) {
    // TODO: Implement invalidatePath() method.
  }

  public function updateHtml(PrerenderedDataInterface $path) {
    // TODO: Implement updateHtml() method.
  }

  public function addCacheInformation($information) {
    // TODO: Implement addCacheInformation() method.
  }

  public function store() {
    // TODO: Implement store() method.
  }

  public function getData($path) {
    // TODO: Implement getData() method.
  }

  public function getDataForCurrentUrl() {
    // TODO: Implement getDataForCurrentUrl() method.
  }

  public function queueData(PrerenderedDataInterface $prerendered_data, $force_update) {
    // TODO: Implement queueData() method.
  }
}
