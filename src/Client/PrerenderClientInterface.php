<?php

namespace Drupal\prerender\Client;

use Symfony\Component\BrowserKit\Request;

interface PrerenderClientInterface {

  /**
   * @inheritdoc
   */
  public function request($method, $uri, array $parameters = [], array $files = [], array $server = [], $content = NULL, $changeHistory = TRUE);

  /**
   * Sends the URL to the prerender service for queueing.
   *
   * @param string $url
   * @return bool
   */
  public function sendRecacheRequest($url);
}