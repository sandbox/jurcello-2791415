<?php
/**
 * @file
 * Contains Drupal\prerender\Client\PrerenderClient
 */

namespace Drupal\prerender\Client;

use Drupal\Core\Config\ConfigFactoryInterface;
use Goutte\Client;

class PrerenderIoClient extends Client implements PrerenderClientInterface {
  const URL = 'http://service.prerender.io/';

  const HEADER_TOKEN = 'HTTP_X-Prerender-Token';
  const HEADER_USER_AGENT = 'HTTP_USER_AGENT';

  const USER_AGENT = 'MediaMonks Prerender';

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Token url.
   *
   * @var string
   */
  protected $token;

  /**
   * The URL of the prerender service.
   *
   * @var string
   */
  protected $serviceUrl;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   */
  public function __construct(ConfigFactoryInterface $config) {
    $this->config = $config;
    $this->token = $this->config->get('prerender.settings')->get('token');
    $this->serviceUrl = $this->config->get('prerender.settings')->get('prerender_endpoint');

    // Set the right prerender.io headers.
    $server[self::HEADER_TOKEN] = $this->token;
    $server[self::HEADER_USER_AGENT] = self::USER_AGENT;
    parent::__construct($server);
  }

  /**
   * @inheritdoc
   */
  public function request($method, $uri, array $parameters = [], array $files = [], array $server = [], $content = null, $changeHistory = true) {
    $url = !empty($this->serviceUrl) ? $this->serviceUrl : self::URL;
    $uri = $url . $uri;
    return parent::request($method, $uri, $parameters, $files, $server, $content, $changeHistory);
  }

  /**
   * @inheritdoc
   */
  public function sendRecacheRequest($url) {
    try {
      // Post a re-cache request to prerender.io to trigger recaching. This
      // will ensure that the actual request done when processing the queue
      // item will not take too long.
      $this->request('POST',
        'http://api.prerender.io/recache', [
          'json' => [
            'prerenderToken' => $this->token,
            'url' => $url,
          ],
        ]);
      return TRUE;
    } catch (\Exception $e) {
      return FALSE;
    }
  }
}