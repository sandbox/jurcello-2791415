<?php

namespace Drupal\prerender;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\prerender\Entity\PrerenderedDataInterface;
use Drupal\prerender\Entity\PrerenderedHtmlEntity;

/**
 * Defines a class to build a listing of Default entity entities.
 *
 * @ingroup prerender
 */
class PrerenderedHtmlEntityListBuilder extends EntityListBuilder {

  use LinkGeneratorTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Default entity ID');
    $header['status'] = $this->t('Status');
    $header['path'] = $this->t('Path');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\prerender\Entity\PrerenderedHtmlEntity */
    $row['id'] = $entity->id();
    $row['status'] = $entity->getStatusDescription();
    $row['name'] = $entity->getTargetPath();
    return $row + parent::buildRow($entity);
  }

  /**
   * @inheritDoc
   */
  public function getOperations(EntityInterface $entity) {
    $operations =  parent::getOperations($entity);
    if ($entity->access('delete') && $entity->hasLinkTemplate('requeue-form')) {
      $operations['requeue'] = array(
        'title' => $this->t('Requeue'),
        'weight' => 100,
        'url' => $entity->toUrl('requeue-form'),
      );
    }
    return $operations;
  }


}
