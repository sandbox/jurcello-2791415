<?php
/**
 * @file
 * Contains Drupal\prerender\Plugin\QueueWorker\CronInvalidate
 */

namespace Drupal\prerender\Plugin\QueueWorker;

use Drupal\prerender\Entity\PrerenderedDataInterface;
use Drupal\prerender\PrerenderServiceInterface;

/**
 * An invalidator.
 *
 * @QueueWorker(
 *   id = "prerender_cron_invalidate",
 *   title = @Translation("Cron Prerenderer invalidator"),
 *   cron = {"time" = 5}
 * )
 */
class CronInvalidate extends CronPrerenderer {
  public function processItem($data) {
    // Load the entity.
    /** @var PrerenderedDataInterface $entity */
    $entity = $this->prerenderedHtmlEntityStorage->load($data->id);
    $entity->setStatus(PrerenderedDataInterface::STATUS_NEEDS_UPDATE);
    $this->prerenderer->queueData($entity);
  }


}