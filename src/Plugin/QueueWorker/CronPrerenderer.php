<?php

namespace Drupal\prerender\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\prerender\Entity\PrerenderedDataInterface;
use Drupal\prerender\PrerenderServiceInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * A prerenderer that pre renders HTML on CRON run.
 *
 * @QueueWorker(
 *   id = "prerender_cron_prerenderer",
 *   title = @Translation("Cron HTML prerenderer"),
 *   cron = {"time" = 20}
 * )
 */
class CronPrerenderer extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $prerenderedHtmlEntityStorage;

  /**
   * The prerenderer.
   *
   * @var PrerenderServiceInterface
   */
  protected $prerenderer;

  /**
   * Creates a new CronPrerenderer object.
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Entity\EntityStorageInterface $entity_storage
   *   The prerendered html storage.
   * @param PrerenderServiceInterface $prerenderer
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityStorageInterface $entity_storage, PrerenderServiceInterface $prerenderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->prerenderedHtmlEntityStorage = $entity_storage;
    $this->prerenderer = $prerenderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.manager')->getStorage('prerendered_html_entity'),
      $container->get('prerender.prerender')
    );
  }

  /**
   * Prerenders the given data and stores the prerendered HTML in the data.
   *
   * @param PrerenderedDataInterface $data
   */
  protected function prerenderData(PrerenderedDataInterface $data) {
    $this->prerenderer->updateHtml($data);
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    // Load the entity.
    /** @var PrerenderedDataInterface $entity */
    $entity = $this->prerenderedHtmlEntityStorage->load($data->id);
    if (!empty($entity)) {
      $this->prerenderData($entity);
    }
  }
}
