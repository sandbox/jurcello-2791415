<?php

namespace Drupal\prerender;

use Drupal\prerender\Entity\PrerenderedDataInterface;
use Drupal\node\Entity\Node;

/**
 * Interface PrerenderServiceInterface.
 *
 * @package Drupal\prerender
 */
interface PrerenderServiceInterface {

  /**
   * Update the html for a specific path.
   *
   * @param PrerenderedDataInterface $prerendered_data
   * @return mixed
   */
  public function updateHtml(PrerenderedDataInterface $prerendered_data);

  /**
   * Queue a path, so on a cron run, the html will be prerendered.
   *
   * @param PrerenderedDataInterface $prerendered_data
   * @param $force_update
   *  Wether the item should be updated.
   * @return mixed
   */
  public function queueData(PrerenderedDataInterface $prerendered_data, $force_update);

  /**
   * Get the prerendered data.
   *
   * @param $path
   * @return PrerenderedDataInterface
   */
  public function getData($path);

  /**
   * Get the data for the current url.
   *
   * @return mixed PrerenderedDataInterface
   */
  public function getDataForCurrentUrl();

  /**
   * Set the crawling to enabled during this request.
   *
   * @param bool $enable
   */
  public function setCrawlingEnabled($enable);

}
