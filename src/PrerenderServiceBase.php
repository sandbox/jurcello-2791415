<?php
/**
 * @file
 * Contains Drupal\prerender\PrerenderServiceBase
 */

namespace Drupal\prerender;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Path\AliasManagerInterface;
use Drupal\Core\Path\PathMatcher;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\prerender\Client\PrerenderClientInterface;
use Drupal\prerender\Entity\PrerenderedDataInterface;
use GuzzleHttp\Client;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class PrerenderServiceBase
 * @package Drupal\prerender
 */
abstract class PrerenderServiceBase  implements PrerenderServiceInterface {
  /**
   * Array of already loaded entities as a static cache.
   *
   * @var array;
   */
  protected static $loadedEntities = [];

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * @var  Client
   */
  protected $prerenderClient;

  /**
   * @var  EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var  RequestStack
   */
  protected $requestStack;

  /**
   * @var  AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * @var QueueFactory
   */
  protected $queueFactory;

  // Variables containing settings.
  /**
   * The token needed by the prerender service.
   *
   * @var string
   */
  protected $token;

  /**
   * The URL of the prerender service.
   * @var string
   */
  protected $serviceUrl;

  /**
   * @var PathMatcher
   */
  protected $pathMatcher;

  /**
   * The CSS selector of the element where the prerendered content needs to be
   * inserted into the DOM.
   *
   * @var string
   */
  protected $cssSelector;

  /**
   * The toggle for enable/disable of the crawler.
   *
   * @var bool
   */
  protected $crawlingEnabled = FALSE;

  /**
   * The time in milliseconds before the prerender cache needs to expire.
   *
   * @var int
   */
  protected $expireTime;

  /**
   * Blacklisted pages.
   *
   * @var string
   */
  protected $blackListedPages;

  /**
   * PrerenderServiceBase constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   * @param \Drupal\prerender\Client\PrerenderClientInterface $http_client
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   * @param \Drupal\Core\Path\AliasManagerInterface $alias_manager
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   */
  public function __construct(ConfigFactoryInterface $config, PrerenderClientInterface $http_client, EntityTypeManagerInterface $entity_type_manager, RequestStack $request_stack, AliasManagerInterface $alias_manager, PathMatcherInterface $path_matcher, QueueFactory $queue_factory) {
    $this->config = $config;
    $this->prerenderClient = $http_client;
    $this->entityTypeManager = $entity_type_manager;
    $this->aliasManager = $alias_manager;
    $this->pathMatcher = $path_matcher;
    $this->requestStack = $request_stack;
    $this->queueFactory = $queue_factory;

    // TODO: maybe these settings should not be stored in protected variables.
    $config = $this->config->get('prerender.settings');
    $this->token = $config->get('token');
    $this->cssSelector = $config->get('css_selector');
    $this->serviceUrl = $config->get('prerender_endpoint');
    $this->blackListedPages = $config->get('blacklisted');
    $this->crawlingEnabled = $config->get('crawl');
    $this->expireTime = $config->get('expire_time');
  }

  /**
   * {@inheritdoc}
   */
  public function queueData(PrerenderedDataInterface $prerendered_data, $force_update = FALSE) {
    if ($this->needsQueueing($prerendered_data, $force_update)) {
      // Re-queue the URL for prerendering.
      $prerendered_data->setStatus(PrerenderedDataInterface::STATUS_QUEUED);
      $url = $prerendered_data->getAbsoluteUrl();
      $requeue = $this->prerenderClient->sendRecacheRequest($url);

      if ($requeue) {
        $prerendered_data->setStatus(PrerenderedDataInterface::STATUS_SEND_TO_PRERENDER);
      }
      else {
        $prerendered_data->setStatus(PrerenderedDataInterface::STATUS_ERROR);
      }

      $prerendered_data->save();
      if ($prerendered_data->getStatus() != PrerenderedDataInterface::STATUS_ERROR) {
        $queue = $this->queueFactory->get('prerender_cron_prerenderer');
        $data = new \stdClass();
        $data->id = $prerendered_data->id();
        $queue->createItem($data);
      }
    }
  }

  /**
   * @inheritdoc
   */
  public function getData($path) {
    // First check if the path is blacklisted.
    // If so, return false.
    if ($this->pathMatcher->matchPath($path, $this->blackListedPages)) {
      return FALSE;
    }
    if (empty(PrerenderServiceBase::$loadedEntities[$path])) {
      $entities = $this->entityTypeManager->getStorage('prerendered_html_entity')
        ->loadByProperties(['pr_path' => $path]);
      if (empty($entities)) {
        PrerenderServiceBase::$loadedEntities[$path] = $this->entityTypeManager->getStorage('prerendered_html_entity')
          ->create([
            'pr_path' => $path,
            'uid' => 0,
            'status' => PrerenderedDataInterface::STATUS_NEW,
          ]);
      }
      else {
        // If there are entities, take the first one, as there should only be one.
        PrerenderServiceBase::$loadedEntities[$path] = array_shift($entities);
      }
    }
    return PrerenderServiceBase::$loadedEntities[$path];
  }

  /**
   * @inheritdoc
   */
  public function getDataForCurrentUrl() {
    // Get the current URL path, this needs to be prefixed with a / because for
    // some unclear reason this is not done by URL::getInternalPath().
    try {
      $current_url = Url::createFromRequest($this->requestStack->getCurrentRequest());
      $path = '/' . $current_url->getInternalPath();
      $prerendered_data = $this->getData($path);
      if ($prerendered_data) {
        // Queue data if anonymous access.
        if ($this->anonymousAccessToUrl($current_url)) {
          $this->queueData($prerendered_data);
        }
        return $prerendered_data;
      }
    } catch (\Exception $e) {
      // If an exception occurs, most probably the current request is a 404.
      // In that case, false should be returned, which is a catch all
      // at the end of this method.
    }
    // If the path is invalid or blacklisted.
    return FALSE;
  }

  /**
   * Check if the anonymous user has access to the current url.
   *
   * @param URL|bool $url
   *  A URL object to check anonymous access to.
   * @return bool
   *  TRUE if the anonymous user has access to the given URL, FALSE otherwise.
   */
  protected function anonymousAccessToUrl(Url $url) {
    $anonymous_user = $this->entityTypeManager->getStorage('user')->load(0);
    return $url->access($anonymous_user);
  }

  /**
   * Asserts if the passed data needs to be queued.
   *
   * @param \Drupal\prerender\Entity\PrerenderedDataInterface $prerendered_data
   * @param bool $force_update
   * @return bool
   */
  public function needsQueueing(
    PrerenderedDataInterface $prerendered_data,
    $force_update
  ) {
    // Check if this data set is accessible to the anonymous user.
    $url = URL::fromUserInput($prerendered_data->getTargetPath());
    if (!$this->anonymousAccessToUrl($url)) {
      $prerendered_data->setStatus(PrerenderedDataInterface::STATUS_IGNORE)
        ->save();
      return FALSE;
    }
    // Check if the data is already queued, if so return FALSE.
    return !in_array($prerendered_data->getStatus(), [
      PrerenderedDataInterface::STATUS_QUEUED,
      PrerenderedDataInterface::STATUS_SEND_TO_PRERENDER,
      PrerenderedDataInterface::STATUS_UPTODATE,
      PrerenderedDataInterface::STATUS_ERROR,
    ]) || ($force_update && !in_array($prerendered_data->getStatus(), [
        PrerenderedDataInterface::STATUS_QUEUED,
        PrerenderedDataInterface::STATUS_SEND_TO_PRERENDER,
      ]));
  }

  /**
   * @inheritDoc
   */
  public function setCrawlingEnabled($enable) {
    $this->crawlingEnabled = $enable;
  }


}