<?php
/**
 * @file
 * Contains Drupal\prerender\PrerenderedHtmlEntityAccessControlHandler
 */

namespace Drupal\prerender;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;


/**
 * Access controller for the prerendered html entity.
 *
 * @see \Drupal\prerender\Entity\PrerenderedHtmlEntity.
 */
class PrerenderedHtmlEntityAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   *
   * Link the activities to the permissions. checkAccess is called with the
   * $operation as defined in the routing.yml file.
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'administer prerendered html');

      case 'edit':
        return AccessResult::allowedIfHasPermission($account, 'administer prerendered html');

      case 'requeue':
        return AccessResult::allowedIfHasPermission($account, 'administer prerendered html');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete prerendered_html entity');
    }
    return AccessResult::allowed();
  }

}