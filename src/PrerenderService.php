<?php

namespace Drupal\prerender;

use Drupal\Core\Path\AliasManager;
use Drupal\Core\Url;
use Drupal\prerender\Entity\PrerenderedDataInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PrerenderService.
 *
 * @package Drupal\prerender
 */
class PrerenderService extends PrerenderServiceBase {

  public function updateHtml(PrerenderedDataInterface $prerendered_data) {
    if ($prerendered_data && (php_sapi_name() == "cli" || strpos($this->requestStack->getCurrentRequest()->headers->get('user_agent'), 'PhantomJS') === FALSE )) {
      // Get the url for the path.
      $url = $prerendered_data->getAbsoluteUrl();

      $prerendered_data->setStatus(PrerenderedDataInterface::STATUS_UPDATING);
      $prerendered_data->save();

      $response = $this->prerenderClient->request(Request::METHOD_GET, $url);

      try {
        if ($response->filter($this->cssSelector)
            ->count() === 1
        ) {
          $full_response = $response->html();
          $data = $response->filter($this->cssSelector)->html();
          if ($this->crawlingEnabled) {
            $this->crawl($response);
          }
          $prerendered_data->setStatus(PrerenderedDataInterface::STATUS_UPTODATE);
          $prerendered_data->setExtractedHtml($data);
          $prerendered_data->setRawResponse($full_response);
        }
        else {
          $prerendered_data->setStatus(PrerenderedDataInterface::STATUS_ERROR);
        }
        $expire = REQUEST_TIME + $this->expireTime;
        $prerendered_data->setExpire($expire);
      } catch (\Exception $e) {
        watchdog_exception('prerender', $e);
        // Set the status of the prerendered_data to error.
        $prerendered_data->setStatus(PrerenderedDataInterface::STATUS_ERROR);
      }
      // Always save the status.
      $prerendered_data->save();
    }
  }

  /**
   * Crawl the website and queue pages for prerendering.
   *
   * @param \Symfony\Component\DomCrawler\Crawler $crawler
   */
  protected function crawl(Crawler $crawler) {
    global $base_url;
    $base_host = parse_url($base_url)['host'];
    /** @var AliasManager $alias_manager */
    $alias_manager = $this->aliasManager;
    $links = $crawler->filter('a')->each( function ( Crawler $node, $i) use ($alias_manager, $base_host) {
      $link =  $node->link()->getUri();
      $parsed_link = parse_url($link);
      if (isset($parsed_link['host']) && $parsed_link['host'] == $base_host) {
        $path = $alias_manager->getPathByAlias($parsed_link['path']);
        return $path;
      }
    });
    // Filter the paths, because some paths should not be crawled.
    // Note that also empty results are filtered.
    $links = array_filter($links, function($value) {
      if (empty($value) || in_array($value, ['/version/src/'])) {
        return FALSE;
      }
      return TRUE;
    });
    $links = array_unique($links);
    foreach ($links as $link) {
      $prerendered_data = $this->getData($link);
      if ($prerendered_data) {
        $this->queueData($prerendered_data);
      }
    }
  }


}
