<?php
/**
 * @file
 * Contains Drupal\prerender\Form\PrerenderedHtmlEntityRequeueConfirm
 */

namespace Drupal\prerender\Form;


use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\prerender\Entity\PrerenderedDataInterface;
use Drupal\prerender\PrerenderServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PrerenderedHtmlEntityRequeueConfirmForm extends ContentEntityConfirmFormBase {
  /** @var  PrerenderServiceInterface */
  protected $prerenderService;

  /**
   * @inheritDoc
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to requeue the prerendered html for %path', ['%path' => $this->entity->getTargetPath()]);
  }

  /**
   * @inheritDoc
   */
  public function getCancelUrl() {
    return new Url('entity.prerendered_html_entity.collection');
  }

  /**
   * @inheritDoc
   */
  public function getConfirmText() {
    return $this->t('Requeue');
  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var PrerenderedDataInterface $entity */
    $entity = $this->getEntity();
    $this->prerenderService->queueData($entity, TRUE);
    $this->logger('prerendered_html_entity')->notice('Requeued %path',
      ['%path' => $entity->getTargetPath()]
      );
    $form_state->setRedirect('entity.prerendered_html_entity.collection');
  }

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager'),
      $container->get('prerender.prerender')
    );
  }

  /**
   * @inheritDoc
   */
  public function __construct( EntityManagerInterface $entity_manager, PrerenderServiceInterface $prerender_service) {
    $this->prerenderService = $prerender_service;
    parent::__construct($entity_manager);
  }


}