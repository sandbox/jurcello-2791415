<?php

namespace Drupal\prerender\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a form for deleting a prerendered_html_entity entity.
 *
 * @ingroup prerendered_html_entity
 */
class PrerenderdHtmlEntityDeleteForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the prerendered html for %path', ['%path' => $this->entity->getTargetPath()]);
  }

  /**
   * {@inheritdoc}
   *
   * If the delete command is canceled, return to the list.
   */
  public function getCancelUrl() {
    return new Url('entity.prerendered_html_entity.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   *
   * Delete the entity and log the event. logger() replaces the watchdog.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $entity->delete();

    $this->logger('prerendered_html_entity')->notice('Deleted %title.',
      array(
        '%title' => $this->entity->label(),
      ));
    $form_state->setRedirect('entity.prerendered_html_entity.collection');
  }

}
