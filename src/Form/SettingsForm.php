<?php

namespace Drupal\prerender\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;


/**
 * Class SettingsForm.
 *
 * @package Drupal\prerender\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'prerender.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'prerender_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('prerender.settings');
    $form['prerender_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prerender.io service url'),
      '#description' => $this->t('Enter the endpoint api url.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('prerender_endpoint'),
    ];
    $form['token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prerender.io token'),
      '#description' => $this->t('The token can be retrieved from the following url: https://prerender.io/account.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('token'),
    ];
    $form['css_selector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Css selector of where content is put in.'),
      '#description' => $this->t('Should be the top element in which the content is injected.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('css_selector'),
    ];
    $form['blacklisted'] = array(
      '#type' => 'textarea',
      '#required' => FALSE,
      '#title' => t('Blacklisted pages'),
      '#description' => t('Specify pages by using their paths. Enter one path per line. The \'*\' character is a wildcard. Example paths are /user for the current user\'s page and /user/* for every user page. <front> is the front page.'),
      '#cols' => 60,
      '#rows' => 5,
      '#default_value' => $config->get('blacklisted'),
    );
    $form['expire_time'] = [
      '#type' => 'number',
      '#title' => $this->t('Expire time'),
      '#description' => $this->t('The time after which the data needs to be fetched again.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('expire_time'),
    ];
    $form['crawl'] = [
      '#type' => 'checkbox',
      '#title' => t('Crawl'),
      '#description' => t('If checked, the prerendered content will be checked on links, and the internal links will be crawled.'),
      '#default_value' => $config->get('crawl'),
    ];
    $form = parent::buildForm($form, $form_state);
    $form['actions']['purge'] = [
      '#type' => 'submit',
      '#name' => 'purge',
      '#value' => t('Purge prerendered data'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggeringElement = $form_state->getTriggeringElement();
    if ($triggeringElement['#name'] == 'purge') {
      $form_state->setRedirect('prerender.purge');
    }
    else {
      parent::submitForm($form, $form_state);
      $this->config('prerender.settings')
        ->set('prerender_endpoint', $form_state->getValue('prerender_endpoint'))
        ->set('token', $form_state->getValue('token'))
        ->set('css_selector', $form_state->getValue('css_selector'))
        ->set('expire_time', $form_state->getValue('expire_time'))
        ->set('blacklisted', $form_state->getValue('blacklisted'))
        ->set('crawl', $form_state->getValue('crawl'))
        ->save();
    }
  }
}
