<?php

namespace Drupal\prerender\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfirmPurgeForm.
 *
 * @package Drupal\prerender\Form
 */
class ConfirmPurgeForm extends ConfirmFormBase {

  /**
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;


  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  public function __construct(EntityTypeManagerInterface $entity_manager) {
    $this->entityTypeManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'confirm_purge_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure to purge all data?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('prerender.prerender_settings_form');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entities = $this->entityTypeManager->getStorage('prerendered_html_entity')->loadMultiple();
    $this->entityTypeManager->getStorage('prerendered_html_entity')->delete($entities);
    drupal_set_message($this->t('The prerendered content was succesfully purged.'));
    $form_state->setRedirect('prerender.prerender_settings_form');
  }


}
