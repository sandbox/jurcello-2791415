<?php

namespace Drupal\prerender\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;

/**
 * Defines the Prerendered html entity entity.
 *
 * @ingroup prerender
 *
 * @ContentEntityType(
 *   id = "prerendered_html_entity",
 *   label = @Translation("Prerendered html entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\prerender\PrerenderedHtmlEntityListBuilder",
 *     "views_data" = "Drupal\prerender\Entity\EntityViewsData",
 *     "form" = {
 *       "delete" = "Drupal\prerender\Form\PrerenderdHtmlEntityDeleteForm",
 *       "requeue" = "Drupal\prerender\Form\PrerenderedHtmlEntityRequeueConfirmForm",
 *     },
 *     "access" = "Drupal\prerender\PrerenderedHtmlEntityAccessControlHandler",
 *   },
 *   base_table = "prerendered_html_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "delete-form" = "/admin/search/prerender/{prerendered_html_entity}/delete",
 *     "requeue-form" = "/admin/search/prerender/{prerendered_html_entity}/requeue",
 *     "collection" = "/admin/search/prerender/list",
 *   }
 * )
 */
class PrerenderedHtmlEntity extends ContentEntityBase implements PrerenderedDataInterface, ContentEntityInterface, EntityChangedInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setExpire($timestamp) {
    $this->set('expire', $timestamp);
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getExpire() {
    return $this->get('expire')->value;
  }


  /**
   * {@inheritdoc}
   */
  public function setExtractedHtml($extractedHtml) {
    $this->set('extracted_html', $extractedHtml);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getExtractedHtml() {
    if ($this->get('extracted_html')->first()) {
      return $this->get('extracted_html')->first()->getValue()['value'];
    }
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function setRawResponse($raw_response) {
    $this->set('raw_response', $raw_response);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status) {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    if ($this->get('status')->first()) {
      return $this->get('status')->first()->getValue()['value'];
    }
    return PrerenderedDataInterface::STATUS_NEW;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetPath() {
    if ($this->get('pr_path')->first()) {
      return $this->get('pr_path')->first()->getValue()['value'];
    }
  }

  /**
   * Get the absolute url for a path.
   *
   * @return \Drupal\Core\GeneratedUrl|string
   */
  public function getAbsoluteUrl() {
    $uri = Url::fromUri('internal:' . $this->getTargetPath());
    $url = $uri->setAbsolute(TRUE)->toString();
    return $url;
  }

  /**
   * @inheritdoc
   */
  public function getRenderArray(RendererInterface $renderer) {
    $render_array = array(
      '#markup' => '<div id="prerender-content">' . $this->getExtractedHtml() . '</div>',
    );
    // Add the cacheble info to the render array for this entity.
    $renderer->addCacheableDependency($render_array, $this);
    return $render_array;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['status'] = BaseFieldDefinition::create('integer')
      ->setSetting('size', 'tiny')
      ->setLabel(t('Update status'))
      ->setDescription(t('A boolean indicating whether the Prerendered html entity needs update.'))
      ->setDefaultValue(TRUE);

    $fields['target_uuid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Target uuid'))
      ->setDescription(t('Uuid of the target entity if there is an entity and the entity has a uuid.'))
      ->setSetting('max_length', 128);

    // Although the name path would be better, it seems to be a reserved keyword.
    // Therefor it is prefixed with pr.
    $fields['pr_path'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Path'))
      ->setDescription(t('The path for which the html is saved.'));

    $fields['cid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Cache cid'))
      ->setDescription(t('The cache cid that belongs to this entity.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['expire'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Expire'))
      ->setDescription(t('The time that the item should expire.'));

    $fields['extracted_html'] = BaseFieldDefinition::create('string_long')
      // Undocumented, but working: when a blob is needed, set case_sensitive to true.
      ->setSetting('case_sensitive', TRUE)
      ->setLabel(t('Extracted html'))
      ->setDescription(t('The rendered html'));

    $fields['raw_response'] = BaseFieldDefinition::create('string_long')
      // Undocumented, but working: when a blob is needed, set case_sensitive to true.
      ->setSetting('case_sensitive', TRUE)
      ->setLabel(t('Raw response'))
      ->setDescription(t('Contains the complete response, including head.'));

    return $fields;
  }

  /**
   * @inheritDoc
   */
  public function getStatusDescription() {
    $descriptions = [
      PrerenderedDataInterface::STATUS_NEW => t('New'),
      PrerenderedDataInterface::STATUS_NEEDS_UPDATE => t('Needs update'),
      PrerenderedDataInterface::STATUS_QUEUED => t('Queued'),
      PrerenderedDataInterface::STATUS_SEND_TO_PRERENDER => t('Send to prerender'),
      PrerenderedDataInterface::STATUS_UPDATING => t('Updating'),
      PrerenderedDataInterface::STATUS_UPTODATE => t('Up to date'),
      PrerenderedDataInterface::STATUS_IGNORE => t('Ignore'),
      PrerenderedDataInterface::STATUS_ERROR => t('Error'),
    ];
    if (isset($descriptions[$this->getStatus()])) {
      return $descriptions[$this->getStatus()];
    }
    return t('Unknown status');
  }


}
