<?php

namespace Drupal\prerender\Entity;

use Drupal\Core\Render\RendererInterface;

/**
 * Provides an interface for defining Prerendered html entity entities.
 *
 * @ingroup prerender
 */
interface PrerenderedDataInterface {

  const STATUS_NEW = 0;
  const STATUS_NEEDS_UPDATE = 1;
  const STATUS_QUEUED = 2;
  const STATUS_SEND_TO_PRERENDER = 3;
  const STATUS_UPDATING = 4;
  const STATUS_UPTODATE = 5;
  const STATUS_IGNORE = 6;
  const STATUS_ERROR = 99;
  // Add get/set methods for your configuration properties here.

  /**
   * Set extracted html.
   *
   * @param $extractedHtml
   */
  public function setExtractedHtml($extractedHtml);

  /**
   * Get the extracted html. This is the html of the main content
   * of the page.
   *
   * @return mixed
   */
  public function getExtractedHtml();

  /**
   * Set the complete raw response, including the head.
   *
   * @param $raw_response
   * @return mixed
   */
  public function setRawResponse($raw_response);

  /**
   * Set the status of the data.
   *
   * @param $status
   *
   * @return self
   */
  public function setStatus($status);

  /**
   * Set the expire time.
   *
   * @param $timestamp
   * @return mixed
   */
  public function setExpire($timestamp);

  /**
   * Get the expire timestamp.
   *
   * @return mixed
   */
  public function getExpire();

  /**
   * Get the status of the data.
   *
   * @return mixed
   */
  public function getStatus();

  /**
   * Get the internal drupal path.
   *
   * @return mixed
   */
  public function getTargetPath();

  /**
   * Get the absolute url for this data.
   *
   * @return mixed
   */
  public function getAbsoluteUrl();

  /**
   * Get a render array that can be added to the page.
   *
   * @param RendererInterface $renderer
   * @return mixed
   */
  public function getRenderArray(RendererInterface $renderer);

  /**
   * Saves the data.
   */
  public function save();

  /**
   * Returns the id.
   *
   * @return mixed
   */
  public function id();

  /**
   * Get the status description.
   *
   * @return string
   */
  public function getStatusDescription();


}
