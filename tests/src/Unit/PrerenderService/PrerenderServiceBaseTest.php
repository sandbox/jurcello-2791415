<?php

namespace Drupal\Tests\prerender\Unit\PrerenderService;


use Drupal\Core\Path\PathMatcher;
use Drupal\Core\Queue\QueueFactory;
use Drupal\prerender\Entity\PrerenderedDataInterface;
use Drupal\prerender\Entity\PrerenderedHtmlEntity;
use Drupal\prerender\PrerenderService;
use Drupal\prerender\PrerenderServiceBase;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\prerender\PrerenderService
 * @group prerender
 */
class PrerenderServiceBaseTest extends UnitTestCase {

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $config;

  /**
   * @var \Drupal\prerender\Client\PrerenderClientInterface|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $prerenderClient;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $entityTypeManager;

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $requestStack;

  /**
   * @var \Drupal\Core\Path\AliasManagerInterface|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $aliasManager;

  /**
   * @var \Drupal\Core\Path\PathMatcherInterface|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $pathMatcher;

  /**
   * @var QueueFactory|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $queueFactory;

  /**
   * @inheritDoc
   */
  protected function setUp() {
    parent::setUp();
    $this->config = $this->getMock('\Drupal\Core\Config\ConfigFactoryInterface');
    $this->prerenderClient = $this->getMock('\Drupal\prerender\Client\PrerenderClientInterface');
    $this->entityTypeManager = $this->getMock('\Drupal\Core\Entity\EntityTypeManagerInterface');
    $this->requestStack = $this->getMock('Symfony\Component\HttpFoundation\RequestStack');
    $this->aliasManager = $this->getMock('\Drupal\Core\Path\AliasManagerInterface');
    $this->pathMatcher = $this->getMock('\Drupal\Core\Path\PathMatcherInterface');
    $this->queueFactory = $this->getMockBuilder('Drupal\Core\Queue\QueueFactory')
      ->disableOriginalConstructor()
      ->getMock();


    $configuration = $this->getMockBuilder('\Drupal\Core\Config\ImmutableConfig')
      ->disableOriginalConstructor()
      ->getMock();
    $configuration->expects($this->any())
      ->method('get')
      ->willReturnMap([
        ['token', 'tokendummy'],
        ['css_selector', 'css_selector'],
        ['prerender_endpoint', 'http://localhost:9000'],
        ['blacklisted', '/user/*'],
        ['crawl', TRUE],
        ['expire_time', 86400],
      ]);

    $this->config->expects($this->any())
      ->method('get')
      ->willReturn($configuration);
  }

  /**
   * @covers ::getData
   * // TODO: use dataprovider
   */
  public function testGetData() {
    // First check a blacklisted page.
    $blacklisted_path = '/user/login';
    $whitelisted_path = '/node/1';
    $loaded_path = '/node/loaded';

    // TODO: atMost should not be used here.
    $this->pathMatcher->expects($this->atMost(4))
      ->method('matchPath')
      ->willReturnMap([
        [$blacklisted_path, '/user/*', TRUE],
        [$whitelisted_path, '/user/*', FALSE],
      ]);

    $prerender_service = new PrerenderService($this->config, $this->prerenderClient, $this->entityTypeManager, $this->requestStack, $this->aliasManager, $this->pathMatcher, $this->queueFactory);


    $this->assertFalse($prerender_service->getData($blacklisted_path));

    // TODO: end of one test.

    // Now proceed into the function.
    // Create the node storage interface.
    $html_entity_storage = $this->getMockBuilder('\Drupal\Core\Entity\EntityStorageInterface')
      ->disableOriginalConstructor()
      ->getMock();

    // Create a new entities.
    $loaded_entity = $this->getMockBuilder('\Drupal\prerender\Entity\PrerenderedHtmlEntity')
      ->disableOriginalConstructor()
      ->getMock();
    $whitelisted_entity = $this->getMockBuilder('\Drupal\prerender\Entity\PrerenderedHtmlEntity')
      ->disableOriginalConstructor()
      ->getMock();

    $html_entity_storage->expects($this->any())
      ->method('loadByProperties')
      ->willReturnMap([
        [['pr_path' => $whitelisted_path], NULL],
        [['pr_path' => $loaded_path], [$loaded_entity]]
      ]);

    $html_entity_storage->expects($this->any())
      ->method('create')
      ->with([
        'pr_path' => $whitelisted_path,
        'uid' => 0,
        'status' => PrerenderedDataInterface::STATUS_NEW
      ])
      ->willReturn($whitelisted_entity);

    $this->entityTypeManager->expects($this->any())
      ->method('getStorage')
      ->willReturn($html_entity_storage);

    $this->assertEquals($loaded_entity, $prerender_service->getData($loaded_path));
    $this->assertEquals($whitelisted_entity, $prerender_service->getData($whitelisted_path));


  }

  /**
   * @covers ::queueData
   */
  public function testQueueDataSuccessfullQueue() {
    // Contstructor arguments.
    $arguments = [
      $this->config,
      $this->prerenderClient,
      $this->entityTypeManager,
      $this->requestStack,
      $this->aliasManager,
      $this->pathMatcher,
      $this->queueFactory,
    ];
    $prerender_service = $this->getMockForAbstractClass('\Drupal\prerender\PrerenderService', $arguments, '', TRUE, TRUE, TRUE, ['needsQueueing']);

    $prerender_service->expects($this->once())
      ->method('needsQueueing')
      ->willReturn(TRUE);

    $this->prerenderClient->expects($this->once())
      ->method('sendRecacheRequest')
      ->willReturn(TRUE);

    // Mock the prerenderedData interface.
    $data = $this->getMockBuilder('\Drupal\prerender\Entity\PrerenderedDataInterface')->getMock();

    // State is mocked here, so it we can be sure that the right state is set.
    $state = PrerenderedDataInterface::STATUS_NEW;
    $data->method('setStatus')
      ->willReturnCallback(function($status) use (&$state) {
        $state = $status;
      });
    $data->method('getStatus')
      ->willReturnCallback(function() use (&$state) {
        return $state;
      });
    $data->expects($this->once())
      ->method('save');

    $data->expects($this->once())
      ->method('getAbsoluteUrl')
      ->willReturn('http://example.com/node/1');

    $queue = $this->getMockBuilder('\Drupal\Core\Queue\QueueInterface')
      ->getMock();

    $this->queueFactory
      ->expects($this->once())
      ->method('get')
      ->willReturn($queue);

    $prerender_service->queueData($data);

    $this->assertEquals(PrerenderedDataInterface::STATUS_SEND_TO_PRERENDER, $data->getStatus());


  }

}